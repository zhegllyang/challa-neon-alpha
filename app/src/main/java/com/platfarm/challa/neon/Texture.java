package com.platfarm.challa.neon;

import java.nio.ByteBuffer;

/**
 * Created by jhsong on 2018-04-03.
 */

public class Texture {
    public ByteBuffer mEncodeData;
    public String mName;
}
