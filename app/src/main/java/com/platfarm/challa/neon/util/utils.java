package com.platfarm.challa.neon.util;

import android.content.Context;
import android.util.TypedValue;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by jhsong on 2018-03-20.
 * 전역 Util 클래스
 */

public class utils {

    /**
     * 코드로 View 생성 시 DP로 설정하기 위한 메소드
     * @param px 픽셀 값
     * @param context 액티비티의 Context
     * @return DP 값
     */
    public static int convertPixelsToDp(float px, Context context) {
        int value = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, px, context.getResources().getDisplayMetrics());
        return value;
    }

    /**
     * 상점의 All pacakge 검색 시 필요한 encoded URI 변환 메소드
     * @param s 일반 스트링
     * @return encode된 스트링
     */
    public static String encodeURIComponent(String s)
    {
        String result = null;

        try
        {
            result = URLEncoder.encode(s, "UTF-8")
                    .replaceAll("\\+", "%20")
                    .replaceAll("\\%21", "!")
                    .replaceAll("\\%27", "'")
                    .replaceAll("\\%28", "(")
                    .replaceAll("\\%29", ")")
                    .replaceAll("\\%7E", "~");
        }

        // This exception should never occur.
        catch (UnsupportedEncodingException e)
        {
            result = s;
        }

        return result;
    }
}
