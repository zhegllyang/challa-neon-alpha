package com.platfarm.challa.neon;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.EmbossMaskFilter;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends Activity {

    public Context mContext = this;
    private boolean mIsSplashLoad;
    private ImageView mSplashImageView;
    private Activity mActivity = this;
    private final int MY_PERMISSION_REQUEST_CODE = 3333;
    public static MainGLSurfaceView view;

    public Context getContext() {
        return mContext;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
        setContentView(new MainGLSurfaceView(mContext));
//        MagicTextView mTextView = findViewById(R.id.magicTextView);
//        mTextView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/RixNeonSignMedium.ttf"));
//        EmbossMaskFilter filter = new EmbossMaskFilter(new float[]{1.5f, 1, 1.5f}, 0.4f, 13, 3.0f);
//        mTextView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
//        mTextView.getPaint().setMaskFilter(filter);

//        mSplashImageView = new ImageView(this);
//        mSplashImageView.setScaleType(ImageView.ScaleType.FIT_XY);
//        mSplashImageView.setImageResource(R.drawable.splash_screen);
//        setContentView(mSplashImageView);
        mIsSplashLoad = true;

//        checkPermission();
    }

    private void checkPermission() {
        if(Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M){
            if(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
                startTemplateActivity(true);
            } else {
                ActivityCompat.requestPermissions(mActivity
                        , new String[]{
                                Manifest.permission.RECORD_AUDIO
                                , Manifest.permission.WRITE_EXTERNAL_STORAGE}
                        , MY_PERMISSION_REQUEST_CODE);
            }
        } else {
            startTemplateActivity(true);
        }
    }

    private void startTemplateActivity(boolean passPermission) {
        int time;
        if(passPermission) {
            time = 2000;
        } else {
            time = 50;
        }
        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                mIsSplashLoad = false;

                Intent i = new Intent(MainActivity.this, TemplateActivity.class);
                startActivity(i);
                finish();
            }
        }, time);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSION_REQUEST_CODE:
                if(grantResults.length > 0) {
                    for(int i = 0; i < grantResults.length; i++) {
                        if(grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        } else if(grantResults[i] == PackageManager.PERMISSION_DENIED) {
                            Toast.makeText(mContext, "권한이 없으면 실행되지 못합니다.", Toast.LENGTH_SHORT).show();
                            finishAffinity();
                            return;
                        }
                    }

                    startTemplateActivity(false);
                }
                break;
        }
    }
}
