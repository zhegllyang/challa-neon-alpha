package com.platfarm.challa.neon;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.EmbossMaskFilter;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by jhsong on 2018-03-08.
 */

public class NeonText {
    private Context mContext;
    private FrameLayout mFrameLayout;
    private int mTextSize = 30;
    private int mFontColor;
    private int mShadowColor;
    private String mStr;
    private ArrayList<CustomTextView> mTextLayer = new ArrayList<>();
    private final static int LAYER_MAX = 5;
    private String mFontPath;
    private boolean isOn = true;
    private Activity mActivity;
    private Timer mTimer;
    private final int layerType = View.LAYER_TYPE_SOFTWARE;
    private final int gravityType = Gravity.CENTER;
    private CustomTextView mNewTextViewShadow;
    private CustomTextView mNewTextViewOff;
    private CustomTextView mStrokeTextView;

    public NeonText(Context context, Activity activity, FrameLayout layout, int fontColor, int shadowColor, String strText, String fontPath) {
        this.mFrameLayout = layout;
        this.mContext = context;
        this.mFontColor = fontColor;
        this.mStr = strText;
        this.mFontPath = fontPath;
        this.mShadowColor = shadowColor;
        this.mActivity = activity;

        // Text Shadow
        mNewTextViewShadow = new CustomTextView(context, false, 1.0f, 0);

        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, Gravity.CENTER);
        mNewTextViewShadow.setLayoutParams(params);
        mNewTextViewShadow.setGravity(gravityType);
        mNewTextViewShadow.setTextSize(TypedValue.COMPLEX_UNIT_SP, mTextSize);
        mNewTextViewShadow.setTextColor(Color.WHITE);
        mNewTextViewShadow.setAlpha(0.5f);
        mNewTextViewShadow.setLayerType(layerType, null);
        mNewTextViewShadow.setShadowLayer(15, 25, 25, Color.parseColor("#202020"));
        mNewTextViewShadow.setText(mStr);
        mNewTextViewShadow.setTypeface(Typeface.createFromAsset(mContext.getAssets(), mFontPath));
        mNewTextViewShadow.setVisibility(View.VISIBLE);
//        newTextViewShadow.setPerspective(true, -20, 30, 300, 300, 0);

        mFrameLayout.addView(mNewTextViewShadow);

        // Text Off
        mNewTextViewOff = new CustomTextView(context, false, 0, 0);

        mNewTextViewOff.setLayoutParams(params);
        mNewTextViewOff.setGravity(gravityType);
        mNewTextViewOff.setTextSize(TypedValue.COMPLEX_UNIT_SP, mTextSize);
        mNewTextViewOff.setTextColor(Color.WHITE);
        mNewTextViewOff.setAlpha(0.1f);
        mNewTextViewOff.setText(mStr);
        mNewTextViewOff.setLayerType(layerType, null);
        mNewTextViewOff.setTypeface(Typeface.createFromAsset(mContext.getAssets(), mFontPath));
        EmbossMaskFilter filter = new EmbossMaskFilter(new float[]{0, 1, 1.5f}, 0.4f, 13, 3.0f);
        mNewTextViewOff.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        mNewTextViewOff.getPaint().setMaskFilter(filter);
        mNewTextViewOff.setVisibility(View.VISIBLE);
//        newTextViewOff.setPerspective(true, -20, 30, 300, 300, 0);

        mFrameLayout.addView(mNewTextViewOff);

        createTurnOnText();
    }

    private void createTurnOnText() {
        mStrokeTextView = new CustomTextView(mContext, true, 3.0f, mFontColor);

        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, Gravity.CENTER);
        mStrokeTextView.setLayoutParams(params);
        mStrokeTextView.setGravity(gravityType);
        mStrokeTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, mTextSize);
        mStrokeTextView.setTextColor(mFontColor);
        mStrokeTextView.setAlpha(0.8f);
        mStrokeTextView.setLayerType(layerType, null);
        mStrokeTextView.setText(mStr);
        mStrokeTextView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), mFontPath));
        mStrokeTextView.setVisibility(View.VISIBLE);

        mStrokeTextView.setShadowLayer(150f, 0, 0, mShadowColor);

//        strokeTextView.setPerspective(true, -20, 30, 300, 300, 0);
        mFrameLayout.addView(mStrokeTextView);

        mTextLayer.add(mStrokeTextView);

        for(int i = 1; i < LAYER_MAX; i++) {
            CustomTextView newTextView = new CustomTextView(mContext, false, 0, Color.parseColor("#ffffff"));

            newTextView.setLayoutParams(params);
            newTextView.setGravity(gravityType);
            newTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, mTextSize);
            newTextView.setTextColor(mFontColor);
            newTextView.setAlpha(0.8f);
            newTextView.setLayerType(layerType, null);
            newTextView.setText(mStr);
            newTextView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), mFontPath));
            newTextView.setVisibility(View.VISIBLE);

            newTextView.setShadowLayer(25f, 0, 0, mShadowColor);
//            newTextView.setPerspective(true, -20, 30, 300, 300, 0);

            mTextLayer.add(newTextView);

            mFrameLayout.addView(mTextLayer.get(i));
        }
    }

    public void turnOffNeon() {
        if(mTextLayer != null && mTextLayer.size() > 0) {
            for(int i = 0; i < mTextLayer.size(); i++) {
                mTextLayer.get(i).setVisibility(View.INVISIBLE);
            }

            isOn = false;
        }
    }

    public void turnOnNeon() {
        if(mTextLayer != null && mTextLayer.size() > 0) {
            for(int i = 0; i < mTextLayer.size(); i++) {
                mTextLayer.get(i).setVisibility(View.VISIBLE);
            }

            isOn = true;
        }
    }

    public void setOnOffTimer(int delay, int milliSecond) {
        final Runnable r = new Runnable() {
            @Override
            public void run() {
                if(!isOn) {
                    turnOnNeon();
                } else {
                    turnOffNeon();
                }

            }
        };

        TimerTask t = new TimerTask() {
            @Override
            public void run() {
                mActivity.runOnUiThread(r);
            }
        };

        mTimer = new Timer();
        mTimer.schedule(t, delay, milliSecond);
    }

    public void setOffTimer() {
        if(mTimer != null) {
            mTimer.cancel();
        }
    }

    public void setLayoutParams(FrameLayout.LayoutParams params) {
        mNewTextViewOff.setLayoutParams(params);
        mNewTextViewShadow.setLayoutParams(params);
        mStrokeTextView.setLayoutParams(params);
        for(int i = 0; i < LAYER_MAX; i++) {
            mTextLayer.get(i).setLayoutParams(params);
        }
    }
}
