package com.platfarm.challa.neon;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.widget.Toast;

import com.matthewtamlin.sliding_intro_screen_library.DotIndicator;

public class TemplateActivity extends FragmentActivity implements TemplatePreviewFragment.OnFragmentInteractionListener{
    private com.platfarm.challa.neon.util.ViewPager mViewPager;
    private PagerAdapter mPagerAdapter;
    private DotIndicator mIndicator;
    private Context mContext = (Context) this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_template);

        mViewPager = findViewById(R.id.template_preview_viewpager);
        mPagerAdapter = new PagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mPagerAdapter);
//        mViewPager.setPageMargin(getResources().getDisplayMetrics().widthPixels / -1);
        mViewPager.setOffscreenPageLimit(2);

        mViewPager.enableCenterLockOfChilds();
        mViewPager.setCurrentItemInCenter(0);

        mIndicator = findViewById(R.id.template_preview_indicator);
        mIndicator.setSelectedDotColor(Color.WHITE);
        mIndicator.setUnselectedDotColor(Color.GRAY);
        mIndicator.setNumberOfItems(5);

        mViewPager.setOnPageChangeListener(new com.platfarm.challa.neon.util.ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mIndicator.setSelectedItem(position, true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    private class PagerAdapter extends FragmentStatePagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return TemplatePreviewFragment.create("테스트\n해봅시다\n" + position, position);
        }

        @Override
        public int getCount() {
            return 5;
        }

        @Override
        public float getPageWidth(int position) {
            return 0.8f;
        }
    }
}
