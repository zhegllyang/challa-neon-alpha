package com.platfarm.challa.neon;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.BlurMaskFilter;
import android.graphics.Color;
import android.graphics.EmbossMaskFilter;
import android.graphics.Typeface;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.MaskFilterSpan;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.platfarm.challa.neon.util.utils;

import java.util.Timer;
import java.util.TimerTask;

public class TemplateEditActivity extends Activity {

    private ImageView mImageView;
    private FrameLayout mLayout;
    private String text;
    private TextView mTextView;
    private TextView mTextView2;
    private Context mContext = (Context) this;
    private String absPath = Environment.getExternalStorageDirectory().getAbsolutePath();
    private String filePath = absPath + "/DCIM/challa/";
    private int mEffectNumber = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_template_edit);

        mLayout = findViewById(R.id.edit_layout);
        mImageView = findViewById(R.id.edit_imageview);
        mTextView = findViewById(R.id.edit_textview);
        mTextView2 = findViewById(R.id.edit_textview2);

        Intent intent = getIntent();
        int type = intent.getIntExtra("type", -1);

        switch (type) {
            case 0:
                Glide.with(mContext).load(filePath + "Bulb_background.gif").into(mImageView);
                float radius = 15.0f;
                BlurMaskFilter filter = new BlurMaskFilter(radius, BlurMaskFilter.Blur.SOLID);
                EmbossMaskFilter filter2 = new EmbossMaskFilter(new float[]{0, 1, 1.5f}, 0.9f, 13, 3.0f);
                mTextView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                mTextView2.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
;
                mTextView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/RixNeonSignMedium.ttf"));
                mTextView2.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/RixNeonSignMedium.ttf"));
//                mTextView.getPaint().setMaskFilter(filter);
                mTextView.setTextColor(Color.WHITE);
                mTextView2.setTextColor(Color.WHITE);
                SpannableString str = new SpannableString(mTextView.getText());
                mTextView.getPaint().setMaskFilter(filter2);
                mTextView2.getPaint().setMaskFilter(filter2);
//                str.setSpan(new ForegroundColorSpan(Color.CYAN), 0, 5, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//                str.setSpan(new MaskFilterSpan(filter2), 0, 3, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//                str.setSpan(new MaskFilterSpan(filter), 0, 2, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                mTextView.setText(str);
                effectNeon();
                break;
        }
    }

    private void effectNeon() {
        TimerTask t = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String string = mTextView.getText().toString();
                        String string2 = mTextView2.getText().toString();

                        if(mEffectNumber > mTextView.getText().length()) {
                            mTextView.setText(string);
                            mTextView2.setText(string2);
                            mEffectNumber = 0;
                        } else {
                            SpannableString str = new SpannableString(mTextView.getText());
                            SpannableString str2 = new SpannableString(string2);
                            float radius = 15.0f;
                            BlurMaskFilter filter = new BlurMaskFilter(radius, BlurMaskFilter.Blur.SOLID);
                            str.setSpan(new ForegroundColorSpan(Color.CYAN), 0, mEffectNumber, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                            str.setSpan(new MaskFilterSpan(filter), 0, mEffectNumber, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                            str2.setSpan(new ForegroundColorSpan(Color.RED), string2.length() - mEffectNumber, string2.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                            str2.setSpan(new MaskFilterSpan(filter), string2.length() - mEffectNumber, string2.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                            mTextView.setText(str);
                            mTextView2.setText(str2);
                            mEffectNumber++;
                        }
                    }
                });
            }
        };

        Timer mTimer = new Timer();
        mTimer.schedule(t, 0, 500);
    }
}
