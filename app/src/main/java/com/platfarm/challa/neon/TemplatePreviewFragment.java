package com.platfarm.challa.neon;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TemplatePreviewFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TemplatePreviewFragment#create} factory method to
 * create an instance of this fragment.
 */
public class TemplatePreviewFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String absPath = Environment.getExternalStorageDirectory().getAbsolutePath();
    private String filePath = absPath + "/DCIM/challa/";

    // TODO: Rename and change types of parameters
    private int mPageNumber;
    private String mString;

    private OnFragmentInteractionListener mListener;

    public TemplatePreviewFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param str Parameter 1.
     * @return A new instance of fragment TemplatePreviewFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TemplatePreviewFragment create(String str, int pageNumber) {
        TemplatePreviewFragment fragment = new TemplatePreviewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, str);
        args.putInt("page", pageNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mString = getArguments().getString(ARG_PARAM1);
            mPageNumber = getArguments().getInt("page");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_template_preview, container, false);

        ImageView imageView = rootView.findViewById(R.id.template_ViewPager_ImageView);
        FrameLayout layout = rootView.findViewById(R.id.template_ViewPager_Layout);

        Glide.with(getContext()).load(filePath + "Bulb_background.gif").into(imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Touch pos : " + mPageNumber, Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getContext(), TemplateEditActivity.class);
                i.putExtra("type", mPageNumber);
                startActivity(i);
            }
        });
//        new NeonText(getContext(), getActivity(), layout, Color.WHITE, Color.parseColor("#1A96FF"), mString, "font/RixNeonSignMedium.ttf").setOnOffTimer(0, 250);

        return rootView;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
