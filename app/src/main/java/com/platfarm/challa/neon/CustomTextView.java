package com.platfarm.challa.neon;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by jhsong on 2018-03-07.
 */

@SuppressLint("AppCompatCustomView")
public class CustomTextView extends TextView {

    private boolean stroke = false;
    private float strokeWidth = 0.0f;
    private int strokeColor;
    private boolean enableSkew = false;
    private float fromDegree;
    private float toDegree;
    private float centerX;
    private float centerY;
    private float depthZ;

    public CustomTextView(Context context) {
        super(context);
    }

    public CustomTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        initView(context, attrs);
    }

    public CustomTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        initView(context, attrs);
    }

    public CustomTextView(Context context, boolean enable, float width, int color) {
        super(context);

        initView(enable, width, color);
    }

    public CustomTextView(Context context, boolean enable, float width, int color, boolean skew, float fromDegree, float toDegree, float centerX, float centerY, float depthZ) {
        super(context);

        initView(enable, width, color, skew, fromDegree, toDegree, centerX, centerY, depthZ);
    }

    private void initView(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomTextView);
        stroke = a.getBoolean(R.styleable.CustomTextView_textStroke, false);
        strokeWidth = a.getFloat(R.styleable.CustomTextView_textStrokeWidth, 0.0f);
        strokeColor = a.getColor(R.styleable.CustomTextView_textStrokeColor, 0xffffffff);
    }

    private void initView(boolean enable, float strokeWidth, int strokeColor) {
        this.stroke = enable;
        this.strokeWidth = strokeWidth;
        this.strokeColor = strokeColor;
    }

    private void initView(boolean enable, float strokeWidth, int strokeColor, boolean skew, float fromDegree, float toDegree, float centerX, float centerY, float depthZ) {
        this.stroke = enable;
        this.strokeWidth = strokeWidth;
        this.strokeColor = strokeColor;
        this.enableSkew = skew;
        this.fromDegree = fromDegree;
        this.toDegree = toDegree;
        this.centerX = centerX;
        this.centerY = centerY;
        this.depthZ = depthZ;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if(stroke) {
            ColorStateList states = getTextColors();
            getPaint().setStyle(Paint.Style.STROKE);
            getPaint().setStrokeWidth(strokeWidth);
            setTextColor(strokeColor);
            super.onDraw(canvas);

            getPaint().setStyle(Paint.Style.FILL);
            setTextColor(states);
        }

//        if(enableSkew) {
//            super.onDraw(canvas);
//            canvas.skew(0.5f, 0.5f);
//            Rotate3dAnimation skew = new Rotate3dAnimation(fromDegree, toDegree, centerX, centerY, depthZ, false);
//            startAnimation(skew);
//        }

        if(!stroke && !enableSkew) {
            super.onDraw(canvas);
        }
    }

    /**
     *
     * @param enableSkew
     * @param fromDegree
     * @param toDegree
     * @param centerX
     * @param centerY
     * @param depthZ
     */
    public void setPerspective(boolean enableSkew, float fromDegree, float toDegree, float centerX, float centerY, float depthZ) {
        this.enableSkew = enableSkew;
        this.fromDegree = fromDegree;
        this.toDegree = toDegree;
        this.centerX = centerX;
        this.centerY = centerY;
        this.depthZ = depthZ;
        invalidate();
    }
}
