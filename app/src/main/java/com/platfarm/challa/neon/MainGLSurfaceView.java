package com.platfarm.challa.neon;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;

/**
 * Created by jhsong on 2018-04-03.
 */

class MainGLSurfaceView extends GLSurfaceView {

    MainGLRenderer mRenderer;

    public MainGLSurfaceView(Context context) {
        super(context);

        mRenderer = new MainGLRenderer(context);
        setEGLContextClientVersion(2);
        setRenderer(mRenderer);
    }

    public MainGLSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
